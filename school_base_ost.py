import sys

school = {}
choices = ['nauczyciel', 'uczen', 'wychowawca', 'koniec']


def input_class_names():
    class_name = input("Podaj nazwę klasy: ")
    class_list = []
    while class_name:
        if class_name not in school:
            school[class_name] = Classroom(class_name)
        class_list.append(class_name)
        class_name = input("Podaj nazwę klasy: ")
    return class_list


class Classroom:
    def __init__(self, class_name: str):
        self.class_name = class_name
        self.students = []
        self.tutor = None
        self.subjects = []
        self.teachers = []


class Student:
    def __init__(self, name, class_name):
        self.name = name
        self.class_name = class_name

    def join_to_class(self):
        if self.class_name not in school:
            school[self.class_name] = Classroom(self.class_name)
        school[self.class_name].students.append(self)


class Teacher:
    def __init__(self, name, subject):
        self.name = name
        self.subject = subject
        self.class_names = []

    def assign_to_classes(self):
        self.class_names = input_class_names()
        for c in self.class_names:
            school[c].teachers.append(self)
            school[c].subjects.append(self.subject)


class Tutor:
    def __init__(self, name):
        self.name = name
        self.class_names = []

    def assign_to_classes(self):
        self.class_names = input_class_names()
        for c in self.class_names:
            school[c].tutor = self


while True:
    user_type = input("Wprowadź typ użytkownika: ")
    if user_type not in choices:
        print("Niedozwolowy wybór. Spróbuj ponownie.")
        continue

    if user_type == "koniec":
        print("Koniec programu")
        break

    name = input("Wprowadź imię i nazwisko: ")
    if user_type == "uczen":
        class_name = input("Twoja klasa to: ")
        pupil = Student(name, class_name)
        pupil.join_to_class()
        continue

    elif user_type == "nauczyciel":
        subject_name = input("Podaj nazwe przedmiotu: ")
        t = Teacher(name, subject_name)
        t.assign_to_classes()
        continue

    elif user_type == "wychowawca":
        person = Tutor(name)
        person.assign_to_classes()
        continue

# Drugi fragment programu
phrase = sys.argv[1:]

if phrase[0] == "nazwa_klasy":
    print("Wybrano tryb programu 'nazwa_klasy'")
    klasa = input("Podaj konkretną nazwę klasy.")
    wychowawca = school[klasa].tutor.name

    if wychowawca:
        print(f"Wychowawca klasy {klasa} jest {wychowawca}")
    else:
        print('Ta klasa aktualnie nie ma wychowawcy')

    uczniowie = []
    for student in school[klasa].students:
        uczniowie.append(student.name)
    print(f"Lista uczniow klasy {klasa}\n{uczniowie}")

elif phrase[0] == "wychowawca":

    print("Wybrano tryb programu 'wychowawca'")
    wychowaca = input("Podaj imie i nazwisko wychowawcy.")
    list_of_pupils = []
    for class_name, class_data in school.items():
        if wychowaca.lower() == class_data.tutor.name.lower():
            list_of_pupils.extend(class_data.students)

    name_of_students = []
    for st in list_of_pupils:
        name_of_students.append(st.name)
    print(f"Lista uczniow prowadzacych przez tego wychowawce {name_of_students}")

elif phrase[0] == "nauczyciel":
    print("Wybrano tryb programu 'nauczyciel'")
    nauczyciel = input("Podaj imie i nazwisko nauczyciela.")
    tutors = []

    for class_name, class_data in school.items():
        if nauczyciel in [t.name for t in class_data.teachers]:

            if class_data.tutor.name:
                wychowawca = class_data.tutor.name
            else:
                wychowawca = 'brak wychowawcy'

            tutors.append(wychowawca)
    print(f"Lista wychowawcow klas w ktorych wybrany nauczyciel prowadzi zajecia\n{tutors}.")

elif phrase[0] == "uczen":
    print("Wybrano tryb programu 'uczen'.")
    uczen = input("Podaj imie i nazwisko ucznia.")
    lekcje = []
    nauczyciele = []

    for class_name, class_data in school.items():
        if uczen in [st.name for st in class_data.students]:
            lekcje.extend(class_data.subjects)
            nauczyciele.extend([t.name for t in class_data.teachers])

    print(f"Uczeń {uczen} ma lekcje {lekcje}")
    print(f"Prowadzone sa przez wymienionych nauczycieli {nauczyciele}")
